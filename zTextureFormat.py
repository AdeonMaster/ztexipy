from enum import IntEnum
from misc import CONFIG_HANDLE
IMAGE_EXT = (".jpg", ".jpeg", ".png", ".tga", ".webp", ".dds", ".bmp")
ZENGIN_EXT = [".tex", ".sav"]  # .SAV = savegame thumbnail

ALL_EXT = ZENGIN_EXT.copy()
ALL_EXT.extend(IMAGE_EXT)

SUPPORTED_WRITE_IMAGE_FORMATS = CONFIG_HANDLE['MISC']["common_image_supported_write"].split() # UI

class zTex(IntEnum):
    ARGB_8888 = 0  # /< \brief 32-bit ARGB pixel format with alpha, using 8 bits per channel
    ABGR_8888 = 1  # /< \brief 32-bit ARGB pixel format with alpha, using 8 bits per channel
    RGBA_8888 = 2  # /< \brief 32-bit ARGB pixel format with alpha, using 8 bits per channel
    BGRA_8888 = 3  # /< \brief 32-bit ARGB pixel format with alpha, using 8 bits per channel
    RGB_888 = 4  # /< \brief 24-bit RGB pixel format with 8 bits per channel
    BGR_888 = 5  # /< \brief 24-bit RGB pixel format with 8 bits per channel
    ARGB_4444 = 6  # /< \brief 16-bit ARGB pixel format with 4 bits for each channel
    ARGB_1555 = 7  # /< \brief 16-bit pixel format where 5 bits are reserved for each color
    # /<  and 1 bit is reserved for alpha
    RGB_565 = 8  # /< \brief 16-bit RGB pixel format with 5 bits for red, 6 bits for green
    # /<  and 5 bits for blue
    PAL_8 = 9  # /< \brief 8-bit color indexed
    DXT1 = 10  # /< \brief DXT1 compression texture format
    DXT2 = 11  # /< \brief DXT2 compression texture format
    DXT3 = 12  # /< \brief DXT3 compression texture format
    DXT4 = 13  # /< \brief DXT4 compression texture format
    DXT5 = 14  # /< \brief DXT5 compression texture format


FORMAT_PRINT_LOOKUP = {
    0: "ARGB8888 (zEnum: 0)",  # rw works
    1: "ABGR8888 (zEnum: 1)",  # rw works
    2: "RGBA8888 (zEnum: 2)",  # rw works
    3: "BGRA8888 (zEnum: 3)",  # rw works
    4: "RGB888 (zEnum: 4)",  # ?
    5: "BGR888 (zEnum: 5)",  # rw works
    6: "ARGB4444 (zEnum: 6)",  # reading works
    7: "ARGB1555 (zEnum: 7)",  # ?
    8: "RGB565 (zEnum: 8)",  # rw works
    9: "Palette8 (zEnum: 9)",  # Gothic Alpha 0.5X, unused in retail
    10: "DXT1 (zEnum: 10)",  # rw works
    11: "DXT2 (zEnum: 11)",  # Not supported by Gothic
    12: "DXT3 (zEnum: 12)",  # rw works
    13: "DXT4 (zEnum: 13)",  # Not supported by Gothic
    14: "DXT5 (zEnum: 14)",  # Not supported by Gothic, rw works
}
