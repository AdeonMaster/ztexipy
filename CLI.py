from concurrent.futures import ProcessPoolExecutor, thread
from multiprocessing import cpu_count
from pathlib import Path
from PIL import Image
from misc import CONFIG_HANDLE
from zTexture import *  # pyinstaller...
from zTextureFormat import ALL_EXT, ZENGIN_EXT

# Multiprocessing is a must because the speed up improvement is like 85%~ (100 textures compresses in like 15 secs)
# from single threaded run (100 textures compresses in 90 secs).
# I've just opted to reduce the core count to around 60% of total available threads.
# I've batch converted Textures/_compiled folder from SoperMachinimaMod that has around 16,4K textures
# and at 12 threads on R5 1600AF it took 189 sec to convert stuff from .tex to .webp.
# Compression to dxtn will be slower (because of the quality flag being set to high)
# but oh well, it'll still nuke through the dataset with respectable results
CPU_USAGE = int(cpu_count() * (0.01 * int(CONFIG_HANDLE["MISC"]["cpu_core_usage"])))

if CPU_USAGE > cpu_count():
    CPU_USAGE = cpu_count()


# Thank you mCoding, you rock! https://www.youtube.com/watch?v=X7vBbelRXn0
def run_with_mp_map(items, function: callable, processes=CPU_USAGE, chunksize=1):
    # ProcessPoolExecutor is a simpler, limited and essentially guarded wrapper around multiprocessing.Pool
    # When running zTEXiPy with multiprocessing.Pool it likes to deadlock with large database, ProcessPoolExecutor does not.
    executor = ProcessPoolExecutor(max_workers=processes)
    try:
        # Timeout makes it so that if the task doesn't finish under 20 sec, we kill the task. Because we never know...
        # Typical conversion from one TEX file takes about 0.1 sec, and compression to dxtn takes about 2 sec for 2048x texture
        # Plenty of time to gracefully kill the hanging process.
        results = executor.map(function, items, chunksize=chunksize, timeout=20)
        executor.shutdown(wait=True)
    except (OSError, KeyboardInterrupt, AttributeError):
        executor.shutdown(wait=False, cancel_futures=True)
        executor._threads.clear()
        thread._threads_queues.clear()
        raise
    return results


def batch_processing(path: Path, path_types, function: callable, parameters: tuple):
    files = tuple(p.resolve() for p in path.iterdir() if p.suffix.lower() in path_types)
    amount_of_files = len(files)
    parameters = [parameters] * amount_of_files
    items = zip(files, parameters)
    items = ((a, *rest) for a, rest in items)
    print(f"Processing {amount_of_files} files")
    return run_with_mp_map(items, function)


class Converter:
    ######################## Main logic
    @staticmethod
    def from_tex(input: Path, format=None, output: Path = None, overwrite=False):
        output_file = None
        if output:
            if output.suffix:
                output_file = output
                output.parent.mkdir(parents=True, exist_ok=True)
                if format:
                    output_file = output.with_suffix(f".{format}")
            else:
                output.mkdir(parents=True, exist_ok=True)
                output_file = output / f"{input.stem.removesuffix('-C')}.{format}"
        else:
            output_file = input.parent / f"{input.stem.removesuffix('-C')}.{format}"

        if output_file.exists():
            if not overwrite:
                print(f"File {output_file} exists... Skipping")
                return False

        ztex = zTexture.load(input, generate_mipmaps=False)
        if ztex:
            ztex[0].save(
                output_file
            )  # always 0, cos zTexture.load(input, ^^^generate_mipmaps=False^^^)
            print(f"Saved: {output_file}")
            return True
        return False

    @staticmethod
    def to_tex(
        input: Path,
        format: int | str = None,
        output: Path = None,
        overwrite=False,
        generate_mipmaps=True,
        size_correction_mode="EXTEND",
    ):
        output_file = None
        if output:
            if output.suffix:
                output.parent.mkdir(parents=True, exist_ok=True)
                output_file = output.parent / f"{output.stem.removesuffix('-C')}-C.TEX"
            else:
                output.mkdir(parents=True, exist_ok=True)
                output_file = output / f"{input.stem.removesuffix('-C')}-C.TEX"
        else:
            output_file = input.parent / f"{input.stem.removesuffix('-C')}-C.TEX"

        if input.name.endswith(".TEX"):
            ztex = zTexture.load(input, generate_mipmaps=False)
            if ztex and ztex[0]:
                zTexture.save(
                    ztex[0],
                    output_file,
                    generate_mipmaps=generate_mipmaps,
                    format_param=format,
                    size_correction_mode=size_correction_mode,
                )
                return True
        else:
            if output_file.exists():
                if not overwrite:
                    print(f"File {output_file} exists... Skipping")
                    return False

            zTexture.save(
                Image.open(input),
                output_file,
                generate_mipmaps=generate_mipmaps,
                format_param=format,
                size_correction_mode=size_correction_mode,
            )
            return True

    ######################## Misc batch setup
    @staticmethod
    def from_tex_batch(input: Path, format=None, output: Path = None, overwrite=False):
        if input is None:
            print("You need to provide output_path for batch conversion!")
            return False
        return batch_processing(
            input, ZENGIN_EXT, Converter.from_tex_mp, (format, output, overwrite)
        )

    @staticmethod
    def to_tex_batch(
        input: Path,
        format: int | str = None,
        output: Path = None,
        overwrite=False,
        generate_mipmaps=True,
        size_correction_mode="EXTEND",
    ):
        if output is None:
            print("You need to provide output_path for batch conversion!")
            return False
        return batch_processing(
            input,
            ALL_EXT,
            Converter.to_tex_mp,
            (format, output, overwrite, generate_mipmaps, size_correction_mode),
        )

    @staticmethod
    def from_tex_mp(args):
        # useful for debug:
        # import os
        # import sys
        # sys.stdout = open(str(os.getpid()) + ".out", "a")
        return Converter.from_tex(*args)

    @staticmethod
    def to_tex_mp(args):
        # useful for debug:
        # import os
        # import sys
        # sys.stdout = open(str(os.getpid()) + ".out", "a")
        return Converter.to_tex(*args)
