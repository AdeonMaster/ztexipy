# Imports were simplifed as much as possible in order to simplify pyinstaller lookup for libs
from ctypes import CDLL, POINTER, c_uint8, c_int
from enum import IntEnum
from misc import graceful_exit, CONFIG_HANDLE, CURRENT_PATH, URL
from numpy import zeros, asarray, uint8
from PIL import Image
from zTextureFormat import zTex
import sys

class squish(IntEnum):
    kDxt1 = 1
    kDxt3 = 2
    kDxt5 = 4

    kColourClusterFit = 8
    kColourRangeFit = 16
    kColourMetricPerceptual = 32
    kColourMetricUniform = 64
    kWeightColourByAlpha = 128
    kColourIterativeClusterFit = 256

FLAGS_LOOKUP = {zTex.DXT1: squish.kDxt1, zTex.DXT3: squish.kDxt3, zTex.DXT5: squish.kDxt5}


ADDITIONAL_DXT_FLAGS = int(CONFIG_HANDLE["LIBSQUISH"]["flags"])


def get_squish():
    path = None
    squish_handle = CONFIG_HANDLE["LIBSQUISH"]["libraryname"]
    error = f"not found! You can get compiled binaries at {URL}/-/tree/main/libsquish-bind/binaries"
    if sys.platform == "linux":
        path = CURRENT_PATH / f"{squish_handle}.so"
        if path and not path.exists():
            graceful_exit(message=f"{squish_handle}.so {error}")
    elif sys.platform == "win32":
        path = CURRENT_PATH / f"{squish_handle}.dll"
        if path and not path.exists():
            graceful_exit(message=f"{squish_handle}.dll {error}")
    else:
        graceful_exit(message=f"System is not supported! {error}")

    return CDLL(str(path))


dll_handle = get_squish()  # Using global scope because of performance reasons.

dll_handle.compress_image.argtypes = (
    POINTER(c_uint8),
    POINTER(c_uint8),
    c_int,
    c_int,
    c_int,
)


def compress_image(image: Image, ztexture_format):
    w, h = image.size

    """
    # Uncomment this if you're gonna use libsquish in another project!
    # Not needed in the project since we're making sure this doesn't happen with DXT.
    if w < 4 or w % 4 != 0 or h < 4 or h % 4 != 0:
        graceful_exit(f"Invalid image - must have dimensions that are multiples of 4.")
    """

    flags = FLAGS_LOOKUP[ztexture_format]

    if flags == squish.kDxt1:
        size = int(max(1, w / 4) * max(1, h / 4) * 8)
    elif flags in (squish.kDxt3, squish.kDxt5):
        size = int(max(1, w / 4) * max(1, h / 4) * 16)

    output = zeros(size).astype(uint8)  # numpy.zeros
    image = asarray(image)  # numpy.asarray

    dll_handle.compress_image(
        image.ctypes.data_as(POINTER(c_uint8)),
        output.ctypes.data_as(POINTER(c_uint8)),
        c_int(w),
        c_int(h),
        c_int(flags + ADDITIONAL_DXT_FLAGS),  # choosing slower, better quality
    )

    return output.tobytes()
