# libsquish-bind for zTEXiPy

Very simple static library exports DXTN compression function, because sadly Pillow lacks DXTN encoding, but that might change in future.

This module is loaded with ctypes at squish.py

## Build requirements

**_NOTE:_**  Some of the requirements like _IDE_ or _compiler_ are just recommendation

In order to compile the module, you have to meet some \
essential requirements,
depending on the target platform.

### Windows

- [Visual Studio 2019](https://visualstudio.microsoft.com/pl/thank-you-downloading-visual-studio/?sku=Community&rel=16)
    
    Visual Studio Components
    * Windows SDK
    * one of the following toolsets, pick one: v140, v141, v142 (recommended v142)
    * (Optional) CMake Tools for Visual Studio
- [CMake 3.17+](https://cmake.org/download/)

### Linux

- g++ compiler
- [CMake 3.17+](https://cmake.org/download/)

## Build instructions

### Windows

#### Visual Studio with CMake tools

- Open a local folder using Visual Studio
- Build the project

#### Visual Studio without CMake tools

- open command line in repo-directory
- type ``mkdir build``
- type ``cd build``
- type ``cmake ..``
- open visual studio .sln and compile the project
- Alternatively if you want to build from command line instead, \
    type ``cmake --build .``
- Be sure you're compiling x64-Release when you're building zTEXiPy with pyinstaller, also make sure to change the name to `libsquish-bind.x64.dll`

### Linux
- Open terminal in repo-directory
- ``mkdir build;cd build``
- Choose your build type from .cmake files. This example will use ``linux-x64.avx2.cmake``
- ``cmake .. -DCMAKE_TOOLCHAIN_FILE=../linux-x64.avx2.cmake && make``
- You should be left with `libsquish-bind.x64.avx2.so` file.