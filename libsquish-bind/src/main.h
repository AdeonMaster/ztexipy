#include <squish.h>

extern "C"
#ifdef _WIN32
__declspec(dllexport)
#endif
void compress_image(unsigned char* input_rgba, unsigned char* compressed_output, int width, int height, int flags);