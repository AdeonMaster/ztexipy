


$startprocessParams = @{
    FilePath     = "$Env:SystemRoot\REGEDIT.exe"
    ArgumentList = '/s', "$($pwd)/clean_registry.reg"
    Verb         = 'RunAs'
    PassThru     = $true
    Wait         = $true
}
$proc = Start-Process @startprocessParams

$input = (gc template.reg)

$path = "\`"$("$($pwd)".Replace("\","\\"))\\zTEXiPy.exe\`""
$path_assoc = "`"$("$($pwd)")\zTEXiPy.exe`""

$input.Replace("INTERNAL_INSTALLATION_PATH", $path) | Out-File -encoding ASCII _temp.reg

cmd /c ftype zengin_texture=
cmd /c assoc .tex=

$startprocessParams = @{
    FilePath     = "$Env:SystemRoot\REGEDIT.exe"
    ArgumentList = '/s', "$($pwd)/_temp.reg"
    Verb         = 'RunAs'
    PassThru     = $true
    Wait         = $true
}

$proc = Start-Process @startprocessParams

cmd /c assoc .tex=zengin_texture
cmd /c ftype zengin_texture=$path_assoc --GUI %1

Remove-Item -Path "$($pwd)/_temp.reg"