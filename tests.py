from zTexture import zTexture
from PIL import Image
from pathlib import Path

# TODO: improve...


def write_test():
    print("Writing test...")
    path = Path("tests\\")
    input_file = path / "1-source.png"
    output_file = path / "temp.TEX"
    ref_file = path / "1-ref.TEX"

    output_file.unlink(missing_ok=True)

    im = Image.open(input_file)
    zTexture.save(im, output_file)

    ref = None
    with open(ref_file, "rb") as fp:
        ref = fp.read()

    output = None
    with open(output_file, "rb") as fp:
        output = fp.read()

    print("Valid:", bytes(output) == bytes(ref))
    output_file.unlink(missing_ok=True)


def read_test():
    print("Reading test...")
    path = Path("tests\\")
    input_file = path / "1-ref.TEX"
    output_file = path / "temp.PNG"
    ref_file = path / "1-ref.png"

    output_file.unlink(missing_ok=True)

    texture = zTexture.load(
        input_file
    )  # texture is a list of all extracted mipmaps - use "len(texture)-1" for last level0 texture
    texture[len(texture) - 1].save(output_file)

    ref = None
    with open(ref_file, "rb") as fp:
        ref = fp.read()

    output = None
    with open(output_file, "rb") as fp:
        output = fp.read()

    print("Valid:", bytes(output) == bytes(ref))
    output_file.unlink(missing_ok=True)


if __name__ == "__main__":
    write_test()
    read_test()
